package com.daily.hotel.needs.hoteldailyneeds.WebService;

import com.daily.hotel.needs.hoteldailyneeds.Pojo.Item;
import com.daily.hotel.needs.hoteldailyneeds.Pojo.DefaultResponse;
import com.daily.hotel.needs.hoteldailyneeds.Pojo.UserInfo;

import java.util.List;

import retrofit2.Call;
import retrofit2.http.Field;
import retrofit2.http.FormUrlEncoded;
import retrofit2.http.GET;
import retrofit2.http.POST;
import retrofit2.http.Path;
import retrofit2.http.Query;


public interface API {
    String Base_URL = "http://nwebzz.com/hotelwork/public/";

    @GET("getItemList")
    Call<List<Item>> getItems(@Query("access_token")String access_token);

    @FormUrlEncoded
    @POST("signIn")
    Call<DefaultResponse> signin(
            @Field("email") String email,
            @Field("password") String password
    );

    @FormUrlEncoded
    @POST("signUp")
    Call<DefaultResponse> signup(
            @Field("name") String name,
            @Field("hotel_name") String hotelname,
            @Field("address") String hoteladd,
            @Field("mobile") String mobnum,
            @Field("email") String email,
            @Field("password") String password
    );

    @GET("getCartItems")
    Call<List<Item>> getcartitems(@Query("access_token")String access_token);

    @POST("addToCart/{item_id}")
    Call<DefaultResponse> addtocart(@Path("item_id") int item_id,@Query("access_token")String access_token,@Query("quantity")String quantity);

    @POST("removeFromCart/{item_id}")
    Call<DefaultResponse> removefromcart(@Path("item_id") int item_id,@Query("access_token")String access_token);

    @GET("getMyProfile")
    Call<UserInfo> getMyProfile(@Query("access_token")String access_token);

    @POST("changeQuantity/{cart_id}")
    Call<DefaultResponse> changeQuantity(@Path("cart_id") int cart_id,@Query("access_token")String access_token,@Query("quantity")String quantity);

    @FormUrlEncoded
    @POST("saveFCMToken")
    Call<DefaultResponse> syncToken(
            @Field("fcm_token") String fcm_token,
            @Field("email") String email
    );

    @POST("placeOrder")
    Call<DefaultResponse> placeOrder(@Query("access_token")String access_token);
}
