package com.daily.hotel.needs.hoteldailyneeds.Activity;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.util.Patterns;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.daily.hotel.needs.hoteldailyneeds.Constants;
import com.daily.hotel.needs.hoteldailyneeds.Pojo.DefaultResponse;
import com.daily.hotel.needs.hoteldailyneeds.R;
import com.daily.hotel.needs.hoteldailyneeds.WebService.API;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

public class SignInActivity extends AppCompatActivity {
    private EditText email_id,pass_word;
    private API api;
    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_sign_in);
        Retrofit retrofit = new Retrofit.Builder()
                .baseUrl(API.Base_URL)
                .addConverterFactory(GsonConverterFactory.create())
                .build();
        api = retrofit.create(API.class);
        final SharedPreferences sharedpreferences = getApplicationContext().getSharedPreferences(Constants.SHARED_PREFS_USER_LOGIN_DETAILS, Context.MODE_PRIVATE);

        android.support.v7.app.ActionBar ab = getSupportActionBar();
        if(ab!=null){
            ab.setTitle("Sign In");
        }

        email_id=findViewById(R.id.et_emailid);
        pass_word=findViewById(R.id.etPassword);
        Button btn_signin = findViewById(R.id.btnsignmenow);

        btn_signin.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                String email= email_id.getText().toString().trim();
                String pass= pass_word.getText().toString().trim();

                if(email.isEmpty()){
                    email_id.setError("Email is Required");
                    email_id.requestFocus();
                    return;
                }
                if(!Patterns.EMAIL_ADDRESS.matcher(email).matches()){
                    email_id.setError("Enter a valid email");
                    email_id.requestFocus();
                    return;
                }
                if(pass.isEmpty()){
                    pass_word.setError("Password Required");
                    pass_word.requestFocus();
                    return;
                }
                if(pass.length()<6) {
                    pass_word.setError("Password should be atleast 6 character long");
                    pass_word.requestFocus();
                    return;
                }

                Call<DefaultResponse> call=api.signin(email,pass);

                call.enqueue(new Callback<DefaultResponse>() {
                    @Override
                    public void onResponse(Call<DefaultResponse> call, Response<DefaultResponse> response) {
                        DefaultResponse resp = response.body();
                        if(resp.getCode() == 200 && resp.getMessage() == null){
                            //Successful LogIn
                            SharedPreferences.Editor editor = sharedpreferences.edit();
                            editor.putBoolean(Constants.KEY_IS_LOGGED_IN, true);
                            editor.putString(Constants.KEY_ACCESS_TOKEN,resp.getToken());
                            editor.apply();
                            String fcmToken = sharedpreferences.getString(Constants.KEY_FCM_TOKEN,null);
                            Boolean isFCMSynced = sharedpreferences.getBoolean(Constants.KEY_IS_FCM_TOKEN_SYNCED,false);
                            if(!isFCMSynced) {
                                syncToken(fcmToken, "onkar@gmail.com");
                            }
                            Intent i = new Intent();
                            setResult(1,i);
                            finish();
                        }
                        else{
                            Toast.makeText(getApplicationContext(),resp.getMessage(),Toast.LENGTH_LONG).show();
                        }
                    }
                    @Override
                    public void onFailure(Call<DefaultResponse> call, Throwable t) {
                        Toast.makeText(getApplicationContext(),t.getMessage(),Toast.LENGTH_LONG).show();
                    }
                });
            }

            private void syncToken(String token,String email) {
                Call<DefaultResponse> call=api.syncToken(token,email);
                call.enqueue(new Callback<DefaultResponse>() {
                    @Override
                    public void onResponse(Call<DefaultResponse> call, Response<DefaultResponse> response) {
                        DefaultResponse resp = response.body();
                        if(resp.getCode() == 200) {
                            SharedPreferences.Editor editor = sharedpreferences.edit();
                            editor.putBoolean(Constants.KEY_IS_FCM_TOKEN_SYNCED, true);
                            editor.apply();
                        }
                    }
                    @Override
                    public void onFailure(Call<DefaultResponse> call, Throwable t) {

                    }
                });
            }
        });
    }
}
