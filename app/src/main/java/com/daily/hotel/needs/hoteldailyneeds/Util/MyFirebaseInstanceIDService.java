package com.daily.hotel.needs.hoteldailyneeds.Util;

import android.content.Context;
import android.content.SharedPreferences;
import android.util.Log;

import com.daily.hotel.needs.hoteldailyneeds.Constants;
import com.google.firebase.iid.FirebaseInstanceId;
import com.google.firebase.iid.FirebaseInstanceIdService;

import static android.content.ContentValues.TAG;

public class MyFirebaseInstanceIDService extends FirebaseInstanceIdService {
    @Override
    public void onTokenRefresh() {
        // Get updated InstanceID token.
        String refreshedToken = FirebaseInstanceId.getInstance().getToken();
        Log.d(TAG, "Refreshed token: " + refreshedToken);
        SharedPreferences sharedpreferences = getApplicationContext().getSharedPreferences(Constants.SHARED_PREFS_USER_LOGIN_DETAILS, Context.MODE_PRIVATE);
        SharedPreferences.Editor editor = sharedpreferences.edit();
        editor.putString(Constants.KEY_FCM_TOKEN, refreshedToken);
        editor.apply();
        // If you want to send messages to this application instance or
        // manage this apps subscriptions on the server side, send the
        // Instance ID token to your app server.
        //sendRegistrationToServer(refreshedToken);
    }
}
