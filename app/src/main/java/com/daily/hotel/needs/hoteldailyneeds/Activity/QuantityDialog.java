package com.daily.hotel.needs.hoteldailyneeds.Activity;

import android.app.Dialog;
import android.content.Context;
import android.content.DialogInterface;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatDialogFragment;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.daily.hotel.needs.hoteldailyneeds.Pojo.DefaultResponse;
import com.daily.hotel.needs.hoteldailyneeds.R;
import com.daily.hotel.needs.hoteldailyneeds.Util.Util;
import com.daily.hotel.needs.hoteldailyneeds.WebService.API;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

public class QuantityDialog extends AppCompatDialogFragment {
    private int unit,item_id;
    private String qnty,token;
    private static final String nums[]= {"Select","0.25","0.50","1.0","1.5","2.0","2.5","3.0"};

    @NonNull
    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {
        AlertDialog.Builder builder=new AlertDialog.Builder(getActivity());
        Util util = new Util();
        token = util.getToken(getActivity());
        LayoutInflater inflater=getActivity().getLayoutInflater();
        View view=inflater.inflate(R.layout.layout_dialog,null);
        final Context mContext = getActivity();
        if (getArguments() != null) {
            unit= getArguments().getInt("measure",1);
            item_id = getArguments().getInt("item_id",1);
        }

        final Spinner spinner = view.findViewById(R.id.spinner);
        ArrayAdapter<String> adapter = new ArrayAdapter<>(getActivity(), android.R.layout.simple_spinner_item,nums);
        adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        spinner.setAdapter(adapter);
        spinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                qnty=parent.getItemAtPosition(position).toString();
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });
        TextView textView_unit = view.findViewById(R.id.tv_unit);
        switch (unit)
        {
            case 1: textView_unit.setText(R.string.tv_kilogram);
                break;
            case 2: textView_unit.setText(R.string.tv_unit);
                break;
            case 3: textView_unit.setText(R.string.tv_dozen);
                break;
            default: textView_unit.setText(null);
        }
        builder.setView(view)
                .setTitle("Quantity")
                .setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {

                    }
                })
                .setPositiveButton("Ok", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        if(!qnty.equalsIgnoreCase("select")) {
                            Retrofit retrofit = new Retrofit.Builder()
                                    .baseUrl(API.Base_URL)
                                    .addConverterFactory(GsonConverterFactory.create())
                                    .build();
                            API api = retrofit.create(API.class);
                            Call<DefaultResponse> call = api.addtocart(item_id, token, qnty);

                            call.enqueue(new Callback<DefaultResponse>() {
                                @Override
                                public void onResponse(Call<DefaultResponse> call, Response<DefaultResponse> response) {
                                    DefaultResponse resp = response.body();
                                    if (resp.getCode() == 200) {
                                        Toast.makeText(mContext, "Added to Cart", Toast.LENGTH_SHORT).show();
                                    }
                                }

                                @Override
                                public void onFailure(Call<DefaultResponse> call, Throwable t) {
                                    Toast.makeText(mContext, "Something went wrong", Toast.LENGTH_SHORT).show();
                                }
                            });
                        }
                        else {
                            Toast.makeText(getActivity(), "Please select quantity", Toast.LENGTH_SHORT).show();
                        }
                     }
                });
        return builder.create();
    }
}
