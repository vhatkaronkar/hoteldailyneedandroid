package com.daily.hotel.needs.hoteldailyneeds.Adapter;

import android.annotation.SuppressLint;
import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.daily.hotel.needs.hoteldailyneeds.Pojo.DefaultResponse;
import com.daily.hotel.needs.hoteldailyneeds.Pojo.Item;
import com.daily.hotel.needs.hoteldailyneeds.R;
import com.daily.hotel.needs.hoteldailyneeds.Util.RecyclerViewItemClickListner;
import com.daily.hotel.needs.hoteldailyneeds.Util.Util;
import com.daily.hotel.needs.hoteldailyneeds.WebService.API;
import com.squareup.picasso.Picasso;

import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;


public class MyCartListAdapter extends  RecyclerView.Adapter<RecyclerView.ViewHolder> {
    private RecyclerViewItemClickListner clickListner;
    private List<Item> MyitemList;
    private Context myContext;
    private float total = 0;
    public MyCartListAdapter(List<Item> MyitemList, Context context) {
        this.MyitemList = MyitemList;
        this.myContext = context;
    }

    public class MyCartItemCardHolder extends RecyclerView.ViewHolder implements View.OnClickListener{
        private TextView item_name,item_price,item_qnty,item_measure;
        private ImageView item_image,Img_Delete,Img_change_qnty;

        private MyCartItemCardHolder(View view) {
            super(view);
            item_name = view.findViewById(R.id.txt_item_name);
            item_image = view.findViewById(R.id.Img_item_imgname);
            item_price =  view.findViewById(R.id.txt_item_price);
            item_qnty=view.findViewById(R.id.txt_changedQnty);
            item_measure=view.findViewById(R.id.txt_measure);
            Img_Delete= view.findViewById(R.id.ImgDelete);
            Img_Delete.setOnClickListener(this);
            Img_change_qnty= view.findViewById(R.id.Img_changeQnty);
            Img_change_qnty.setOnClickListener(this);
   }

        @Override
        public void onClick(View view) {
            if(clickListner!=null){
                    clickListner.onClick(view,getAdapterPosition());
            }
        }
    }

    private class cartTotalCard extends RecyclerView.ViewHolder  implements View.OnClickListener{
        private TextView cartTotal;
        private Button button_place_order;

        private cartTotalCard(View itemView) {
            super(itemView);
            cartTotal = itemView.findViewById(R.id.total_cart);
            button_place_order=itemView.findViewById(R.id.btn_placeOrder);
            button_place_order.setOnClickListener(this);
        }

        @Override
        public void onClick(View view) {
            if(clickListner!=null){
                clickListner.onClick(view,getAdapterPosition());
            }
        }
    }

    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        switch (viewType){
            case 0: View v1 = LayoutInflater.from(parent.getContext())
                    .inflate(R.layout.cart_total_cardview, parent, false);
                    return new cartTotalCard(v1);
            case 1 : View v2 = LayoutInflater.from(parent.getContext())
                    .inflate(R.layout.my_item_cardview, parent, false);
                    return new MyCartItemCardHolder(v2);
            default:return null;
        }

    }

    @SuppressLint("SetTextI18n")
    @Override
    public void onBindViewHolder(final RecyclerView.ViewHolder holder, int position) {
        if(holder instanceof cartTotalCard){
            final cartTotalCard totalHolder = (cartTotalCard) holder;
            totalHolder.cartTotal.setText(myContext.getResources().getString(R.string.rupee_symbol)+total);
            total = 0;
        }
        else if(holder instanceof MyCartItemCardHolder){
            final Item item = MyitemList.get(position);
            final MyCartItemCardHolder itemHolder = (MyCartItemCardHolder) holder;
            total = total + item.getPrice()*Float.parseFloat(item.getQuantity());
            itemHolder.item_name.setText(item.getName());
            Picasso.with(myContext)
                    .load("http://nwebzz.com/hotelwork/Assets/Images/"+item.getImageName())
                    .into(itemHolder.item_image);
            itemHolder.item_price.setText(myContext.getResources().getString(R.string.rupee_symbol)+item.getPrice()*Float.parseFloat(item.getQuantity()));
            itemHolder.item_qnty.setText(item.getQuantity());
            itemHolder.item_measure.setText(String.valueOf(item.getMeasure()));
        }
    }

    @Override
    public int getItemViewType(int position) {
        return position == MyitemList.size() ? 0 : 1;
    }

    @Override
    public int getItemCount() {
        return MyitemList.size() == 0 ? 0 : MyitemList.size() + 1;
    }

    public void setClickListner(RecyclerViewItemClickListner clickListner){
        this.clickListner = clickListner;
    }
}
