package com.daily.hotel.needs.hoteldailyneeds.Fragment;

import android.content.Context;
import android.net.Uri;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.DividerItemDecoration;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;

import com.daily.hotel.needs.hoteldailyneeds.Activity.MainActivity;
import com.daily.hotel.needs.hoteldailyneeds.Activity.QuantityDialog;
import com.daily.hotel.needs.hoteldailyneeds.Adapter.ItemListAdapter;
import com.daily.hotel.needs.hoteldailyneeds.Pojo.Item;
import com.daily.hotel.needs.hoteldailyneeds.R;
import com.daily.hotel.needs.hoteldailyneeds.Util.RecyclerViewItemClickListner;
import com.daily.hotel.needs.hoteldailyneeds.Util.Util;
import com.daily.hotel.needs.hoteldailyneeds.WebService.API;

import java.util.ArrayList;
import java.util.List;

import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

public class NewOrderFragment extends Fragment implements RecyclerViewItemClickListner {
    // TODO: Rename parameter arguments, choose names that match
    // the fragment initialization parameters, e.g. ARG_ITEM_NUMBER
    private static final String ARG_PARAM1 = "param1";
    private static final String ARG_PARAM2 = "param2";

    // TODO: Rename and change types of parameters
    private String mParam1;
    private String mParam2;
    List<Item> itemList = new ArrayList<>();
    private ItemListAdapter itemListAdapter;
    private OnFragmentInteractionListener mListener;

    public NewOrderFragment() {
        // Required empty public constructor
    }
    // TODO: Rename and change types and number of parameters
    public static NewOrderFragment newInstance(String param1, String param2) {
        NewOrderFragment fragment = new NewOrderFragment();
        Bundle args = new Bundle();
        args.putString(ARG_PARAM1, param1);
        args.putString(ARG_PARAM2, param2);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        itemListAdapter = new ItemListAdapter(itemList,getActivity());
        Util util = new Util();
        String token = util.getToken(getContext());
        Retrofit retrofit = new Retrofit.Builder()
                .baseUrl(API.Base_URL)
                .addConverterFactory(GsonConverterFactory.create())
                .build();
        API api = retrofit.create(API.class);
        Call<List<Item>> call = api.getItems(token);

        call.enqueue(new Callback<List<Item>>() {
            @Override
            public void onResponse(Call<List<Item>> call, Response<List<Item>> response) {
                itemList.addAll(response.body());
//                for (Item i: itemList){
//                    itemList.add(new Item(i.getId(),i.getName(),i.getImageName(),i.getPrice()));
//                }
                itemListAdapter.notifyDataSetChanged();
            }

            @Override
            public void onFailure(Call<List<Item>> call, Throwable t) {
                Toast.makeText(getActivity(),"Something went wrong",Toast.LENGTH_SHORT).show();
            }
        });
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_new_order, container, false);
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        RecyclerView itemListRecyclerView;
        itemListRecyclerView = view.findViewById(R.id.itemListForNewOrderRecyclerView);
        RecyclerView.LayoutManager mLayoutManager = new LinearLayoutManager(getActivity());
        itemListRecyclerView.setLayoutManager(mLayoutManager);
        itemListRecyclerView.setItemAnimator(new DefaultItemAnimator());
        itemListRecyclerView.addItemDecoration(new DividerItemDecoration(getActivity(), LinearLayoutManager.VERTICAL));
        itemListRecyclerView.setAdapter(itemListAdapter);
        itemListAdapter.setClickListner(this);
    }

    // TODO: Rename method, update argument and hook method into UI event
    public void onButtonPressed(Uri uri) {
        if (mListener != null) {
            mListener.onFragmentInteraction(uri);
        }
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        if (context instanceof OnFragmentInteractionListener) {
            mListener = (OnFragmentInteractionListener) context;
        } else {
            throw new RuntimeException(context.toString()
                    + " must implement OnFragmentInteractionListener");
        }
    }

    @Override
    public void onDetach() {
        super.onDetach();
        mListener = null;
    }

    @Override
    public void onClick(View view, final int position) {
        Util util = new Util();
        if(util.getLogInStatusValue(getContext())) {
            int measure=itemList.get(position).getMeasure();
            int item_id=itemList.get(position).getId();
            int addedInCart=itemList.get(position).getAddedInCart();
            if (addedInCart==0) {
                QuantityDialog quantityDialog = new QuantityDialog();
                Bundle bundle = new Bundle();
                bundle.putInt("measure", measure);
                bundle.putInt("item_id", item_id);
                quantityDialog.setArguments(bundle);
                quantityDialog.show(((MainActivity) getContext()).getSupportFragmentManager(), "Quantity Dialog");
            }
            else {
                Toast.makeText(getContext(),"Already Added To Cart", Toast.LENGTH_SHORT).show();
            }
        }
        else {
            Toast.makeText(getContext(), "First Sign In to Your Account", Toast.LENGTH_SHORT).show();
        }
    }

    public interface OnFragmentInteractionListener {
        // TODO: Update argument type and name
        void onFragmentInteraction(Uri uri);
    }
}
