package com.daily.hotel.needs.hoteldailyneeds.Fragment;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.net.Uri;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.daily.hotel.needs.hoteldailyneeds.Activity.MainActivity;
import com.daily.hotel.needs.hoteldailyneeds.Activity.SignInActivity;
import com.daily.hotel.needs.hoteldailyneeds.Activity.SignUpActivity;
import com.daily.hotel.needs.hoteldailyneeds.Constants;
import com.daily.hotel.needs.hoteldailyneeds.Pojo.UserInfo;
import com.daily.hotel.needs.hoteldailyneeds.R;
import com.daily.hotel.needs.hoteldailyneeds.Util.Util;
import com.daily.hotel.needs.hoteldailyneeds.WebService.API;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;


public class SignInFragment extends Fragment {
    private OnFragmentInteractionListener mListener;
    private Util util = new Util();

    public SignInFragment() {
        // Required empty public constructor
    }
    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        util = new Util();
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.fragment_sign_in, container, false);
        RelativeLayout userProfile = rootView.findViewById(R.id.logged_in_user_profile);
        LinearLayout loginButtons = rootView.findViewById(R.id.login_buttons);
        if(!(util.getLogInStatusValue(getActivity()))){
            userProfile.setVisibility(View.GONE);
            loginButtons.setVisibility(View.VISIBLE);
        }
        else{
            TextView userName, userHotel, userMobile;
            userName = rootView.findViewById(R.id.user_name);
            userMobile = rootView.findViewById(R.id.user_phone);
            userHotel = rootView.findViewById(R.id.user_hotel_name);
            userProfile.setVisibility(View.VISIBLE);
            loginButtons.setVisibility(View.GONE);
            setUserDetails(userName,userMobile,userHotel);
        }
        Button btn_signin = rootView.findViewById(R.id.openSignIn);
        TextView txtNoAcc = rootView.findViewById(R.id.tvNotAcc);
        Button btn_log_out = rootView.findViewById(R.id.btn_log_out);
        btn_log_out.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                SharedPreferences sharedpreferences = getActivity().getSharedPreferences(Constants.SHARED_PREFS_USER_LOGIN_DETAILS, Context.MODE_PRIVATE);
                SharedPreferences.Editor editor = sharedpreferences.edit();
                editor.remove(Constants.KEY_IS_LOGGED_IN);
                editor.remove(Constants.KEY_ACCESS_TOKEN);
                editor.apply();
                Intent i = new Intent(getActivity(),MainActivity.class);
                getActivity().finish();
                startActivity(i);
            }
        });
        btn_signin.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent=new Intent(getContext(),SignInActivity.class);
                startActivityForResult(intent,1);
            }
        });
        txtNoAcc.setOnClickListener(new View.OnClickListener(){
            @Override
            public void onClick(View view) {
                Intent intent=new Intent(getContext(),SignUpActivity.class);
                startActivityForResult(intent,1);
            }
        });
        return rootView;
    }

    private void setUserDetails(final TextView userName, final TextView userMobile, final TextView userHotel) {
        Retrofit retrofit = new Retrofit.Builder()
                .baseUrl(API.Base_URL)
                .addConverterFactory(GsonConverterFactory.create())
                .build();
        API api = retrofit.create(API.class);
        String token = util.getToken(getActivity());
        Call<UserInfo> call=api.getMyProfile(token);

        call.enqueue(new Callback<UserInfo>() {
            @Override
            public void onResponse(Call<UserInfo> call, Response<UserInfo> response) {
                UserInfo resp = response.body();
                if(resp.getName()!=null){
                    userName.setText("Hi, "+resp.getName());
                    userHotel.setText(resp.getHotel_name());
                    userMobile.setText(resp.getMobile());
                }
            }
            @Override
            public void onFailure(Call<UserInfo> call, Throwable t) {
                Toast.makeText(getActivity(),t.getMessage(),Toast.LENGTH_SHORT).show();
            }
        });
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if(requestCode ==1 && resultCode == 1){
            Toast.makeText(getActivity(),"Login Successfful",Toast.LENGTH_LONG).show();
            Intent i = new Intent(getActivity(),MainActivity.class);
            getActivity().finish();
            startActivity(i);
        }
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        if (context instanceof OnFragmentInteractionListener) {
            mListener = (OnFragmentInteractionListener) context;
        } else {
            throw new RuntimeException(context.toString()
                    + " must implement OnFragmentInteractionListener");
        }
    }

    @Override
    public void onDetach() {
        super.onDetach();
        mListener = null;
    }

    /**
     * This interface must be implemented by activities that contain this
     * fragment to allow an interaction in this fragment to be communicated
     * to the activity and potentially other fragments contained in that
     * activity.
     * <p>
     * See the Android Training lesson <a href=
     * "http://developer.android.com/training/basics/fragments/communicating.html"
     * >Communicating with Other Fragments</a> for more information.
     */
    public interface OnFragmentInteractionListener {
        // TODO: Update argument type and name
        void onFragmentInteraction(Uri uri);
    }
}
