package com.daily.hotel.needs.hoteldailyneeds;

public class Constants {
    //Shared Prefs
    public static final String SHARED_PREFS_USER_LOGIN_DETAILS = "shared_prefs_user_login_details";
    public static final String KEY_ACCESS_TOKEN = "key_access_token";
    public static final String KEY_IS_LOGGED_IN = "key_current_login_status";
    public static final String KEY_FCM_TOKEN = "key_fcm_token";
    public static final String KEY_IS_FCM_TOKEN_SYNCED = "key_is_fcm_token_synced";
}
