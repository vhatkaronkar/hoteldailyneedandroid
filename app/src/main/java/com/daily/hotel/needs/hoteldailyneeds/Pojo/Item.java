package com.daily.hotel.needs.hoteldailyneeds.Pojo;

/**
 * Created by Dell on 3/26/2018.
 *
 */

public class Item {
    private int cart_id;
    private int id;
    private String name;
    private String imageName;
    private int price;
    private int measure;
    private String quantity;
    private int addedInCart;

    public Item(int cart_id,int id, String name, String imageName, int price, int measure, String quantity,int addedInCart) {
        this.cart_id = cart_id;
        this.id = id;
        this.name = name;
        this.imageName = imageName;
        this.price = price;
        this.measure=measure;
        this.quantity=quantity;
        this.addedInCart=addedInCart;
    }

    public int getCart_id() {
        return cart_id;
    }

    public int getId() {
        return id;
    }

    public String getName() {
        return name;
    }

    public String getImageName() {
        return imageName;
    }

    public int getPrice() { return price; }

    public int getMeasure() { return measure; }

    public String getQuantity() {
        return quantity;
    }

    public int getAddedInCart() {
        return addedInCart;
    }

    public void setQuantity(String quantity) {
        this.quantity = quantity;
    }

    public void setPrice(int price) {
        this.price = price;
    }
}
