package com.daily.hotel.needs.hoteldailyneeds.Adapter;

import android.content.Context;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.daily.hotel.needs.hoteldailyneeds.Activity.MainActivity;
import com.daily.hotel.needs.hoteldailyneeds.Activity.QuantityDialog;
import com.daily.hotel.needs.hoteldailyneeds.Pojo.Item;
import com.daily.hotel.needs.hoteldailyneeds.R;
import com.daily.hotel.needs.hoteldailyneeds.Util.RecyclerViewItemClickListner;
import com.daily.hotel.needs.hoteldailyneeds.Util.Util;
import com.squareup.picasso.Picasso;

import java.util.List;

public class ItemListAdapter extends RecyclerView.Adapter<ItemListAdapter.ItemCardHolder>{
    private List<Item> itemList;
    private Context mContext;
    private RecyclerViewItemClickListner clickListner;

    public class ItemCardHolder extends RecyclerView.ViewHolder implements View.OnClickListener {
        private TextView item_name,item_price;
        private ImageView item_image,Img_AddToCart;

        private ItemCardHolder(View view) {
            super(view);
            item_name = view.findViewById(R.id.txt_item_name);
            item_image = view.findViewById(R.id.Img_item_imgname);
            item_price =  view.findViewById(R.id.txt_item_price);
            Img_AddToCart= view.findViewById(R.id.ImgAddToCart);
            Img_AddToCart.setOnClickListener(this);
        }

        @Override
        public void onClick(View view) {
            if(clickListner!=null){
                clickListner.onClick(view,getAdapterPosition());
            }
        }
    }

    public ItemListAdapter(List<Item> itemList, Context context) {
        this.itemList = itemList;
        this.mContext = context;
    }

    @Override
    public ItemCardHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.item_cardview, parent, false);

        return new ItemCardHolder(itemView);
    }

    @Override
    public void onBindViewHolder(ItemCardHolder holder, int position) {
        final Item item = itemList.get(position);
        holder.item_name.setText(item.getName());
        Picasso.with(mContext)
                .load("http://nwebzz.com/hotelwork/Assets/Images/"+item.getImageName())
                .into(holder.item_image);
        holder.item_price.setText("Rs."+item.getPrice());
    }

    @Override
    public int getItemCount() {
        return itemList.size();
    }

    public void setClickListner(RecyclerViewItemClickListner clickListner) {
        this.clickListner = clickListner;
    }
}
