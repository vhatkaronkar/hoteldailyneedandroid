package com.daily.hotel.needs.hoteldailyneeds.Adapter;

import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentStatePagerAdapter;

import com.daily.hotel.needs.hoteldailyneeds.Fragment.NewOrderFragment;
import com.daily.hotel.needs.hoteldailyneeds.Fragment.RecentOrdersFragment;
import com.daily.hotel.needs.hoteldailyneeds.Fragment.SignInFragment;

/**
 * Created by Dell on 3/26/2018.
 *
 */

public class PagerAdapterHome extends FragmentStatePagerAdapter {
    private int noOfTabs;

    public PagerAdapterHome(FragmentManager fm, int tabCount){
        super(fm);
        this.noOfTabs =tabCount;
    }
    @Override
    public Fragment getItem(int position) {
        switch (position){
            case 0:
                return new RecentOrdersFragment();
            case 1:
                return new NewOrderFragment();
            case 2:
                return new SignInFragment();
            default:
                return null;
        }
    }

    @Override
    public int getCount() {
        return noOfTabs;
    }
}
