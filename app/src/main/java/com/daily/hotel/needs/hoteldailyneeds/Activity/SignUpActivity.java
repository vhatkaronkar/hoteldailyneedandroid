package com.daily.hotel.needs.hoteldailyneeds.Activity;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Patterns;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.daily.hotel.needs.hoteldailyneeds.Constants;
import com.daily.hotel.needs.hoteldailyneeds.Pojo.DefaultResponse;
import com.daily.hotel.needs.hoteldailyneeds.R;
import com.daily.hotel.needs.hoteldailyneeds.WebService.API;

import java.io.IOException;

import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

public class SignUpActivity extends AppCompatActivity {
    private EditText et_name, et_email, et_pass, et_conpass,et_hotelName,et_HotelAdd,et_MobNum;
    private API api;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_sign_up);

        android.support.v7.app.ActionBar ab = getSupportActionBar();
        if(ab!=null){
            ab.setTitle("Sign Up");
        }

        et_name = findViewById(R.id.etname);
        et_hotelName=findViewById(R.id.etHotelname);
        et_HotelAdd=findViewById(R.id.etHotelAdd);
        et_MobNum=findViewById(R.id.etMobNum);
        et_email = findViewById(R.id.etemailid);
        et_pass = findViewById(R.id.etpass);
        et_conpass = findViewById(R.id.etconpass);
        Button btn_SignUp = findViewById(R.id.btnSignUp);
        btn_SignUp.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                usersignup();
            }
        });
        Retrofit retrofit = new Retrofit.Builder()
                .baseUrl(API.Base_URL)
                .addConverterFactory(GsonConverterFactory.create())
                .build();
        api = retrofit.create(API.class);
    }

    private void usersignup() {
        String name = et_name.getText().toString().trim();
        String hotelname = et_hotelName.getText().toString().trim();
        String hoteladd = et_HotelAdd.getText().toString().trim();
        String mobnum = et_MobNum.getText().toString().trim();
        String email = et_email.getText().toString().trim();
        String pass = et_pass.getText().toString().trim();
        String conpass = et_conpass.getText().toString().trim();

        if (name.isEmpty()) {
            et_name.setError("Name is Required");
            et_name.requestFocus();
            return;
        }
        if (hotelname.isEmpty()) {
            et_hotelName.setError("Hotel Name is Required");
            et_hotelName.requestFocus();
            return;
        }
        if (hoteladd.isEmpty()) {
            et_HotelAdd.setError("Hotel Address is Required");
            et_HotelAdd.requestFocus();
            return;
        }
        if (mobnum.isEmpty()) {
            et_MobNum.setError("Mobile Number is Required");
            et_MobNum.requestFocus();
            return;
        }
        if (!Patterns.PHONE.matcher(mobnum).matches()) {
            et_MobNum.setError("Enter a valid Mobile Number");
            et_MobNum.requestFocus();
            return;
        }
        if (mobnum.length() != 10) {
            et_MobNum.setError("Mobile Number Should Contain 10 Digits");
            et_MobNum.requestFocus();
            return;
        }
        if (email.isEmpty()) {
            et_email.setError("Email is Required");
            et_email.requestFocus();
            return;
        }
        if (!Patterns.EMAIL_ADDRESS.matcher(email).matches()) {
            et_email.setError("Enter a valid email");
            et_email.requestFocus();
            return;
        }
        if (pass.isEmpty()) {
            et_pass.setError("Password Required");
            et_pass.requestFocus();
            return;
        }
        if (pass.length() < 6) {
            et_pass.setError("Password should be atleast 6 character long");
            et_pass.requestFocus();
            return;
        }
        if (conpass.isEmpty()) {
            et_conpass.setError("Password is Required to be Confirmed");
            et_conpass.requestFocus();
            return;
        }
        if(!pass.equals(conpass)){
            et_conpass.setError("Password Not Matched");
            et_conpass.requestFocus();
            return;
        }
        Call<DefaultResponse> call=api.signup(name,hotelname,hoteladd,mobnum,email,pass);

        call.enqueue(new Callback<DefaultResponse>() {
            @Override
            public void onResponse(Call<DefaultResponse> call, Response<DefaultResponse> response) {
                    DefaultResponse resp = response.body();
                    if(resp.getCode() == 200){
                        Toast.makeText(getApplicationContext(),"Sign Up Successful",Toast.LENGTH_SHORT).show();
                        SharedPreferences sharedpreferences = getApplicationContext().getSharedPreferences(Constants.SHARED_PREFS_USER_LOGIN_DETAILS, Context.MODE_PRIVATE);
                        SharedPreferences.Editor editor = sharedpreferences.edit();
                        editor.putBoolean(Constants.KEY_IS_LOGGED_IN, true);
                        editor.putString(Constants.KEY_ACCESS_TOKEN,resp.getToken());
                        editor.apply();
                        Intent i = new Intent();
                        setResult(1,i);
                        finish();
                    }
                 }
            @Override
            public void onFailure(Call<DefaultResponse> call, Throwable t) {
                Toast.makeText(getApplicationContext(),t.getMessage(),Toast.LENGTH_SHORT).show();
            }
        });
    }
}