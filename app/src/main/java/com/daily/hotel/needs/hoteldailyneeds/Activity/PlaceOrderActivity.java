package com.daily.hotel.needs.hoteldailyneeds.Activity;

import android.annotation.SuppressLint;
import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

import com.daily.hotel.needs.hoteldailyneeds.R;

public class PlaceOrderActivity extends AppCompatActivity {
    private int order_id;
    @SuppressLint("SetTextI18n")
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_place_order);

        Bundle extras = getIntent().getExtras();
        if (extras != null) {
            order_id = extras.getInt("order_id");
        }
        TextView textView_showorderid = findViewById(R.id.showorderid);
        textView_showorderid.setText(Integer.toString(order_id));

        Button button_contShop = findViewById(R.id.btn_conti_shop);
        button_contShop.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(PlaceOrderActivity.this, MainActivity.class));
            }
        });
    }

    @Override
    public void onBackPressed() {

    }
}
