package com.daily.hotel.needs.hoteldailyneeds.Util;

import android.view.View;

/**
 * Created by Dell on 8/16/2018.
 */

public interface RecyclerViewItemClickListner {
    public void onClick(View v, int position);
}
