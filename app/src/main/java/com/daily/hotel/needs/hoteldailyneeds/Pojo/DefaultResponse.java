package com.daily.hotel.needs.hoteldailyneeds.Pojo;

public class DefaultResponse {
    private int code;
    private String message;
    private String token;
    private int order_id;

    public DefaultResponse(int code, String message, String token,int order_id) {
        this.code = code;
        this.message = message;
        this.token = token;
        this.order_id=order_id;
    }

    public int getOrder_id() {
        return order_id;
    }

    public int getCode() {
        return code;
    }

    public String getMessage() {
        return message;
    }

    public String getToken() {
        return token;
    }
}
