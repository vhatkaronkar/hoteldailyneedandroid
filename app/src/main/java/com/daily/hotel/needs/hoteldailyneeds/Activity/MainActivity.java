package com.daily.hotel.needs.hoteldailyneeds.Activity;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.net.Uri;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.TabLayout;
import android.support.v4.view.PagerAdapter;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.Toast;

import com.daily.hotel.needs.hoteldailyneeds.Adapter.PagerAdapterHome;
import com.daily.hotel.needs.hoteldailyneeds.Constants;
import com.daily.hotel.needs.hoteldailyneeds.Fragment.NewOrderFragment;
import com.daily.hotel.needs.hoteldailyneeds.Fragment.RecentOrdersFragment;
import com.daily.hotel.needs.hoteldailyneeds.Fragment.SignInFragment;
import com.daily.hotel.needs.hoteldailyneeds.R;

public class MainActivity extends AppCompatActivity implements SignInFragment.OnFragmentInteractionListener,NewOrderFragment.OnFragmentInteractionListener,RecentOrdersFragment.OnFragmentInteractionListener{
    private String quantity;
    private static final int NewOrderFragment= 0;
    private static final int RecentOrdersFragment = 1;
    private static final int SignInFragment = 2;
    FloatingActionButton fab;

    public void setQuantity(String q) {
        this.quantity = q;
        Toast.makeText(getApplicationContext(),"Selected : "+quantity,Toast.LENGTH_SHORT).show();
    }

    public String getQuantity() {
        return quantity;
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        TabLayout tabLayout = findViewById(R.id.tabLayout);
        tabLayout.addTab(tabLayout.newTab().setIcon(R.drawable.ic_recent_order) );
        tabLayout.addTab(tabLayout.newTab().setIcon(R.drawable.ic_new_order) );
        tabLayout.addTab(tabLayout.newTab().setIcon(R.drawable.ic_user_outline)  );
        tabLayout.setTabGravity(TabLayout.GRAVITY_FILL);

        final ViewPager pager = findViewById(R.id.pagerHome);
        final PagerAdapter radapter = new PagerAdapterHome(getSupportFragmentManager(),tabLayout.getTabCount());
        pager.setAdapter(radapter);
        pager.setOnPageChangeListener(new TabLayout.TabLayoutOnPageChangeListener(tabLayout));
        tabLayout.setOnTabSelectedListener(new TabLayout.OnTabSelectedListener() {
            @Override
            public void onTabSelected(TabLayout.Tab tab) {
                pager.setCurrentItem(tab.getPosition());
            }

            @Override
            public void onTabUnselected(TabLayout.Tab tab) {

            }

            @Override
            public void onTabReselected(TabLayout.Tab tab) {

            }
        });

        pager.addOnPageChangeListener(new ViewPager.SimpleOnPageChangeListener(){
            @Override
            public void onPageSelected(int position) {
                if (position==2)
                {
                    fab.setVisibility(View.GONE);
                }
                else
                {
                    fab.setVisibility(View.VISIBLE);
                }
            }
        }
        );

        fab=findViewById(R.id.fab);
        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                SharedPreferences sharedpreferences = getApplicationContext().getSharedPreferences(Constants.SHARED_PREFS_USER_LOGIN_DETAILS, Context.MODE_PRIVATE);
                Boolean logInStatusValue = sharedpreferences.getBoolean(Constants.KEY_IS_LOGGED_IN,false);
                if(logInStatusValue){
                    startActivity(new Intent(MainActivity.this, MyCartActivity.class));
                }
                else
                {
                    Toast.makeText(getApplicationContext(),"First Sign In to your Account",Toast.LENGTH_SHORT).show();
                }

            }
        });
    }

    @Override
    public void onFragmentInteraction(Uri uri) {

    }
}
