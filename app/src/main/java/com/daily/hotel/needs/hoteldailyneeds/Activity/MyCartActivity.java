package com.daily.hotel.needs.hoteldailyneeds.Activity;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.DividerItemDecoration;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.View;
import android.widget.Toast;

import com.daily.hotel.needs.hoteldailyneeds.Adapter.ItemListAdapter;
import com.daily.hotel.needs.hoteldailyneeds.Adapter.MyCartListAdapter;
import com.daily.hotel.needs.hoteldailyneeds.Pojo.DefaultResponse;
import com.daily.hotel.needs.hoteldailyneeds.Pojo.Item;
import com.daily.hotel.needs.hoteldailyneeds.R;
import com.daily.hotel.needs.hoteldailyneeds.Util.RecyclerViewItemClickListner;
import com.daily.hotel.needs.hoteldailyneeds.Util.Util;
import com.daily.hotel.needs.hoteldailyneeds.WebService.API;

import java.util.ArrayList;
import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

public class MyCartActivity extends AppCompatActivity implements RecyclerViewItemClickListner{
    private static final String TAG="";
    List<Item> MyitemList = new ArrayList<>();
    private MyCartListAdapter myCartListAdapter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_mycart);
        android.support.v7.app.ActionBar ab = getSupportActionBar();
        if(ab!=null){
            ab.setTitle("My Cart");
        }
        RecyclerView myitemListRecyclerView;
        myitemListRecyclerView=findViewById(R.id.MyitemListForOrderRecyclerView);
        myCartListAdapter = new MyCartListAdapter(MyitemList,MyCartActivity.this);
        RecyclerView.LayoutManager mLayoutManager= new LinearLayoutManager(getApplicationContext());
        myitemListRecyclerView.setLayoutManager(mLayoutManager);
        myitemListRecyclerView.setItemAnimator(new DefaultItemAnimator());
        myitemListRecyclerView.addItemDecoration(new DividerItemDecoration(MyCartActivity.this, LinearLayoutManager.VERTICAL));
        myitemListRecyclerView.setAdapter(myCartListAdapter);
        myCartListAdapter.setClickListner(this);

        Retrofit retrofit = new Retrofit.Builder()
                .baseUrl(API.Base_URL)
                .addConverterFactory(GsonConverterFactory.create())
                .build();
        API api = retrofit.create(API.class);

        Util util = new Util();
        String token = util.getToken(getApplicationContext());
        Call<List<Item>> call = api.getcartitems(token);

        call.enqueue(new Callback<List<Item>>() {
            @Override
            public void onResponse(Call<List<Item>> call, Response<List<Item>> response) {
                MyitemList.addAll(response.body());
//                for (Item i: itemList){
//                    itemList.add(new Item(i.getId(),i.getName(),i.getImageName(),i.getPrice()));
//                }
                myCartListAdapter.notifyDataSetChanged();
            }
            @Override
            public void onFailure(Call<List<Item>> call, Throwable t) {
                Toast.makeText(MyCartActivity.this,"Something went wrong",Toast.LENGTH_SHORT).show();
            }
        });
     }

    @Override
    public void onClick(View view, final int position) {
        if(view.getId()==R.id.Img_changeQnty){
            Util util = new Util();
            if(util.getLogInStatusValue(this)) {
                int measure=MyitemList.get(position).getMeasure();
                int cart_id=MyitemList.get(position).getCart_id();
                QuantityDialogChangeQnty quantityDialogChangeQnty=new QuantityDialogChangeQnty();
                Bundle bundle = new Bundle();
                bundle.putInt("position", position);
                bundle.putInt("measure",measure);
                bundle.putInt("cart_id",cart_id);
                quantityDialogChangeQnty.setArguments(bundle);
                quantityDialogChangeQnty.show((MyCartActivity.this).getSupportFragmentManager(),"Quantity Dialog");
            }
            else {
                Toast.makeText(this, "First Sign In to Your Account", Toast.LENGTH_SHORT).show();
            }
        }
        else if(view.getId()==R.id.ImgDelete){
            Util util = new Util();
            String token = util.getToken(getApplicationContext());
            int item_id = MyitemList.get(position).getId();
            Retrofit retrofit = new Retrofit.Builder()
                    .baseUrl(API.Base_URL)
                    .addConverterFactory(GsonConverterFactory.create())
                    .build();
            API api = retrofit.create(API.class);
            Call<DefaultResponse> call = api.removefromcart(item_id,token);

            call.enqueue(new Callback<DefaultResponse>() {
                @Override
                public void onResponse(Call<DefaultResponse> call, Response<DefaultResponse> response) {
                    DefaultResponse resp = response.body();
                    if (resp.getCode() == 200) {
                        Toast.makeText(getApplicationContext(), "Removed from Cart: "+MyitemList.get(position).getName(), Toast.LENGTH_SHORT).show();
                        MyitemList.remove(position);
                        myCartListAdapter.notifyDataSetChanged();
                    }
                    if (resp.getCode() != 200) {
                        Toast.makeText(getApplicationContext(), "Unable to delete,try later", Toast.LENGTH_SHORT).show();
                    }
                }
                @Override
                public void onFailure(Call<DefaultResponse> call, Throwable t) {
                    Toast.makeText(getApplicationContext(), "Something went wrong", Toast.LENGTH_SHORT).show();
                }
            });
        }
        else if (view.getId()==R.id.btn_placeOrder){
            Util util = new Util();
            String token = util.getToken(getApplicationContext());
            Retrofit retrofit = new Retrofit.Builder()
                    .baseUrl(API.Base_URL)
                    .addConverterFactory(GsonConverterFactory.create())
                    .build();
            API api = retrofit.create(API.class);
            Call<DefaultResponse> call = api.placeOrder(token);

            call.enqueue(new Callback<DefaultResponse>() {
                @Override
                public void onResponse(Call<DefaultResponse> call, Response<DefaultResponse> response) {
                    DefaultResponse resp = response.body();
                    if (resp.getCode() == 200) {
                        int order_id=resp.getOrder_id();
                        Intent intent=new Intent(MyCartActivity.this,PlaceOrderActivity.class);
                        Bundle bundle=new Bundle();
                        bundle.putInt("order_id",order_id);
                        intent.putExtras(bundle);
                        startActivity(intent);
                        Toast.makeText(getApplicationContext(), "Order Placed Successfully", Toast.LENGTH_SHORT).show();
                    }
                    if (resp.getCode() != 200) {
                        Toast.makeText(getApplicationContext(), "Unable Place Your Order,try later", Toast.LENGTH_SHORT).show();
                    }
                }
                @Override
                public void onFailure(Call<DefaultResponse> call, Throwable t) {
                    Toast.makeText(getApplicationContext(), "Something went wrong", Toast.LENGTH_SHORT).show();
                }
            });
        }
    }

    public void changeQuantity(int position,String qnty) {
        Log.d(TAG, "notifyData");
        final Item item = MyitemList.get(position);
        item.setQuantity(qnty);
        myCartListAdapter.notifyDataSetChanged();
    }
}

