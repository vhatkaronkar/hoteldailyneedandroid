package com.daily.hotel.needs.hoteldailyneeds.Util;

import android.content.Context;
import android.content.SharedPreferences;

import com.daily.hotel.needs.hoteldailyneeds.Constants;

/**
 * Created by SHREE on 31/07/2018.
 */

public class Util {

    public Boolean getLogInStatusValue(Context context) {
        SharedPreferences sharedpreferences = context.getSharedPreferences(Constants.SHARED_PREFS_USER_LOGIN_DETAILS, Context.MODE_PRIVATE);
        Boolean logInStatusValue = sharedpreferences.getBoolean(Constants.KEY_IS_LOGGED_IN,false);
        return logInStatusValue;
    }

    public String getToken(Context context) {
        SharedPreferences sharedpreferences = context.getSharedPreferences(Constants.SHARED_PREFS_USER_LOGIN_DETAILS, Context.MODE_PRIVATE);
        String token = sharedpreferences.getString(Constants.KEY_ACCESS_TOKEN,null);
        return token;
    }
}
