package com.daily.hotel.needs.hoteldailyneeds.Pojo;

/**
 * Created by Dell on 8/7/2018.
 */

public class UserInfo {
    private int id;
    private String name;
    private String address;
    private String hotel_name;
    private String mobile;
    private String email;

    public UserInfo(int id, String name, String address, String hotel_name, String mobile, String email) {
        this.id = id;
        this.name = name;
        this.address = address;
        this.hotel_name = hotel_name;
        this.mobile = mobile;
        this.email = email;
    }

    public int getId() {
        return id;
    }

    public String getName() {
        return name;
    }

    public String getAddress() {
        return address;
    }

    public String getHotel_name() {
        return hotel_name;
    }

    public String getMobile() {
        return mobile;
    }

    public String getEmail() {
        return email;
    }
}
